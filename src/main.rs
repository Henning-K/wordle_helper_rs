use std::fs::File;
use std::io::{BufRead, BufReader};
use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long, default_value_t = String::from(". . . . ."), help = "Start excluded entries with `^`, known characters at that position with just that character and positions with unknown possibilities with `.`")]
    positions: String,

    #[clap(short, long, default_value_t = String::new())]
    known_chars_without_positions: String,

    #[clap(short, long, default_value_t = String::new())]
    excluded: String,
}

enum Position {
    Unknown,
    Known(char),
    Excluded(Vec<char>),
}

fn main() {
    let args = Args::parse();

    let dict_path = "/etc/dictionaries-common/words";
    let dict_file = File::open(dict_path).unwrap();
    let dict_reader = BufReader::new(dict_file);

    let positions = args
        .positions
        .split_whitespace()
        .map(|x| match x {
            "." => Position::Unknown,
            _ => {
                if x.starts_with("^") {
                    Position::Excluded(x.chars().skip(1).collect::<Vec<_>>())
                } else {
                    Position::Known(x.chars().next().unwrap())
                }
            }
        })
        .collect::<Vec<_>>();

    let matches: Vec<_> = dict_reader
        .lines()
        .map(|x| x.unwrap())
        .filter(|x| {
            x.chars()
                .all(|c| c.is_ascii_alphabetic() && c.is_lowercase())
        })
        .filter(|x| x.trim().len() == 5)
        .filter(|x| {
            args.known_chars_without_positions
                .chars()
                .all(|c| x.contains(c))
        })
        .filter(|x| {
            !args.excluded.chars().any(|c| x.contains(c))
        })
        .filter(|x| {
            x.chars().enumerate().all(|(i, c)| match &positions[i] {
                Position::Unknown => true,
                Position::Known(a) => c == *a,
                Position::Excluded(a) => !a.contains(&c),
            })
        })
        .collect();

    for i in &matches {
        println!("{}", i);
    }
    let result_len = matches.len();
    println!(
        "######## {} item{} ########",
        result_len,
        if result_len > 1 { "s" } else { "" }
    );
}
